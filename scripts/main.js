function changeStyling() {
  const classes = getClassesToSet();
  const elSquares = document.querySelectorAll('div.table div.cell');
  for (let i = 0; i < elSquares.length; i++) {
    const elSquare = elSquares[i];
    elSquare.setAttribute('class', `cell ${classes}`);
  }
}

function getClassesToSet() {
  let classes = '';
  // Element angewählter Farboption ermitteln
  const elColorChecked = document.querySelector(
    '#form-style input[name=op-colour]:checked'
  );
  // Klasse für Farbe setzen, wenn eine angewählt
  if (elColorChecked) {
    classes += elColorChecked.value + ' ';
  }
  // Elemente angewählter Textformatierungen ermitteln
  const elTextsChecked = document.querySelectorAll(
    '#form-style #op-bold:checked, #form-style #op-italic:checked'
  );
  // Klasse für angewählte Textformatierung setzen
  for (let i = 0; i < elTextsChecked.length; i++) {
    const elTextChecked = elTextsChecked[i];
    classes += elTextChecked.value + ' ';
  }
  return classes;
}
